{
  description = "image-indexer";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
        };
      in
      {
        packages = rec {
          image-indexer = pkgs.callPackage ./image-indexer.nix {
            inherit (pkgs.darwin.apple_sdk.frameworks) Security;
          };

          default = image-indexer;
        };

        apps = rec {
          dev = flake-utils.lib.mkApp { drv = self.packages.${system}.image-indexer; };
          default = dev;
        };

        devShell = with pkgs; mkShell {
          nativeBuildInputs = [
            cargo
            cargo-outdated
            cargo-zigbuild
            clippy
            gcc
            imagemagick
            rust-analyzer
            rustc
            rustfmt
            taplo
          ];

          RUST_SRC_PATH = "${pkgs.rust.packages.stable.rustPlatform.rustLibSrc}";
        };
      });
}
