use std::path::Path;

use args::Args;
use clap::Parser;
use magick::{
    json::{MagickJson, PerceptualHashJson},
    MagickError,
};
use rayon::prelude::{IntoParallelRefIterator, ParallelIterator};
use repo::{ImageRepo, RepoError};
use vectordb::{TreeError, Vector, VectorDb};

mod args;
mod magick;
mod repo;

struct State {
    grayscale: VectorDb<7>,
    fullcolor: VectorDb<42>,
    grayscale_repo: ImageRepo,
    fullcolor_repo: ImageRepo,
}

struct SimilarImages {
    grayscale: Vec<(f32, String)>,
    fullcolor: Vec<(f32, String)>,
}

#[derive(Debug)]
enum Error {
    Io(std::io::Error),
    Magick(MagickError),
    Repo(RepoError),
    Vector(TreeError),
    PathString,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = Args::parse();

    let db_directory = Path::new("./repo");

    let grayscale = VectorDb::open(db_directory.join("grayscale"), 4)?;
    let fullcolor = VectorDb::open(db_directory.join("fullcolor"), 4)?;
    let grayscale_repo = ImageRepo::open(db_directory.join("grayscale-repo"))?;
    let fullcolor_repo = ImageRepo::open(db_directory.join("fullcolor-repo"))?;

    let state = State {
        grayscale,
        fullcolor,
        grayscale_repo,
        fullcolor_repo,
    };

    for path in &args.index {
        if !path.is_absolute() {
            eprintln!("Paths must be absolute");
            return Ok(());
        }

        state.visit_path(path)?;
    }

    if let Some(query) = &args.query {
        if !query.is_file() {
            eprintln!("Query image is not a file");
            return Ok(());
        }

        if !query.is_absolute() {
            eprintln!("Paths must be absolute");
            return Ok(());
        }

        let SimilarImages {
            grayscale,
            fullcolor,
        } = state.find_similar_images(query)?;

        println!("Grayscale similarities:");
        for (score, path) in grayscale {
            println!("score {score:.5}: {path}");
        }

        println!();
        println!("Fullcolor similarities:");
        for (score, path) in fullcolor {
            println!("score {score:.5}: {path}");
        }
    }

    Ok(())
}

fn to_array(values: &PerceptualHashJson) -> [[f32; 2]; 7] {
    [
        values.ph1, values.ph2, values.ph3, values.ph4, values.ph5, values.ph6, values.ph7,
    ]
}

fn create_fullcolor_vectors(json: &[MagickJson]) -> Vec<Vector<42>> {
    json.iter()
        .filter_map(|json| {
            let channel0 = to_array(json.image.channel_perceptual_hash.hashes.get("Channel0")?);
            let channel1 = to_array(json.image.channel_perceptual_hash.hashes.get("Channel1")?);
            let channel2 = to_array(json.image.channel_perceptual_hash.hashes.get("Channel2")?);

            let array: [f32; 42] = channel0
                .into_iter()
                .chain(channel1.into_iter())
                .chain(channel2.into_iter())
                .flatten()
                .collect::<Vec<_>>()
                .try_into()
                .expect("Correct dimensionality");

            Some(Vector::from(array))
        })
        .collect()
}

fn create_grayscale_vectors(json: &[MagickJson]) -> Vec<Vector<7>> {
    json.iter()
        .filter_map(|json| {
            if json.image.type_ != "Grayscale" {
                return None;
            }

            let hashes = to_array(json.image.channel_perceptual_hash.hashes.get("Channel0")?);

            let vector = Vector::from(hashes.map(|[first, _]| first));

            Some(vector)
        })
        .collect()
}

fn find_similar_images<const N: usize>(
    vectordb: &VectorDb<N>,
    repo: &ImageRepo,
    threshold: Option<f32>,
    vectors: &[Vector<N>],
) -> Result<Vec<(f32, String)>, Error> {
    let out = vectors
        .par_iter()
        .map(|vector| {
            let similar_vectors =
                vectordb.find_similarities_blocking(vector.clone(), threshold, 10)?;

            let mut output = Vec::with_capacity(vectors.len());

            for similar_vector_id in similar_vectors {
                let Some(similar_vector) = vectordb.get_vector_blocking(similar_vector_id)? else {
                continue;
            };

                let similarity = vector.squared_euclidean_distance(&similar_vector);

                let Some(name) = repo.get_name(similar_vector_id)? else {
                continue;
            };

                output.push((similarity, name));
            }

            Ok(output)
        })
        .collect::<Result<Vec<Vec<_>>, Error>>()?
        .into_iter()
        .flatten()
        .collect();

    Ok(out)
}

fn insert_grayscale_vectors(
    vectordb: &VectorDb<7>,
    repo: &ImageRepo,
    name: &str,
    json: &[MagickJson],
) -> Result<(), Error> {
    let vectors = create_grayscale_vectors(json);

    if vectors.is_empty() {
        return Ok(());
    }

    let ids = vectordb.insert_vectors_blocking(vectors)?;

    repo.add_image(name, &ids)?;

    Ok(())
}

fn insert_fullcolor_vectors(
    vectordb: &VectorDb<42>,
    repo: &ImageRepo,
    name: &str,
    json: &[MagickJson],
) -> Result<(), Error> {
    let vectors = create_fullcolor_vectors(json);

    if vectors.is_empty() {
        return Ok(());
    }

    let ids = vectordb.insert_vectors_blocking(vectors)?;

    repo.add_image(name, &ids)?;

    Ok(())
}

fn get_existing_vectors<const N: usize>(
    vectordb: &VectorDb<N>,
    repo: &ImageRepo,
    name: &str,
) -> Result<Vec<Vector<N>>, Error> {
    let vector_ids = repo.get_vectors(name)?;

    let mut out = Vec::with_capacity(vector_ids.len());

    for id in vector_ids {
        if let Some(vector) = vectordb.get_vector_blocking(id)? {
            out.push(vector);
        }
    }

    Ok(out)
}

impl State {
    fn find_similar_images(&self, path: &Path) -> Result<SimilarImages, Error> {
        let name = path.to_str().ok_or_else(|| Error::PathString)?;

        let mut existing_fullcolor =
            get_existing_vectors(&self.fullcolor, &self.fullcolor_repo, name)?;

        let mut existing_grayscale =
            get_existing_vectors(&self.grayscale, &self.grayscale_repo, name)?;

        if existing_fullcolor.is_empty() {
            let json = magick::identify_fullcolor(path)?;

            for json in json {
                if json.image.type_ == "Grayscale" {
                    existing_grayscale.extend(create_grayscale_vectors(&[json]));
                } else {
                    existing_fullcolor.extend(create_fullcolor_vectors(&[json]));
                }
            }
        }

        if existing_grayscale.is_empty() {
            let json = magick::identify_grayscale(path)?;

            existing_grayscale.extend(create_grayscale_vectors(&json));
        }

        let grayscale = find_similar_images(
            &self.grayscale,
            &self.grayscale_repo,
            Some(0.18),
            &existing_grayscale,
        )?;

        let fullcolor = find_similar_images(
            &self.fullcolor,
            &self.fullcolor_repo,
            Some(20.0),
            &existing_fullcolor,
        )?;

        Ok(SimilarImages {
            grayscale,
            fullcolor,
        })
    }

    fn visit_path(&self, path: &Path) -> Result<(), Error> {
        if path.is_dir() {
            self.visit_dirs(path)
        } else if path.is_file() {
            self.visit_file(path)
        } else {
            // don't handle symlinks
            Ok(())
        }
    }

    fn visit_dirs(&self, dir: &Path) -> Result<(), Error> {
        let entries = std::fs::read_dir(dir)?.collect::<Result<Vec<_>, _>>()?;

        entries
            .par_iter()
            .map(|entry| {
                let path = entry.path();
                self.visit_path(&path)
            })
            .collect::<Result<(), _>>()
    }

    fn visit_file(&self, path: &Path) -> Result<(), Error> {
        let name = path.to_str().ok_or_else(|| Error::PathString)?;

        if name.ends_with("xcf") || name.ends_with("psd") {
            return Ok(());
        }

        if self.fullcolor_repo.image_exists(name)? && self.grayscale_repo.image_exists(name)? {
            return Ok(());
        }

        let json = match magick::identify_fullcolor(path) {
            Ok(json) => json,
            Err(e) => {
                eprintln!("{e}");
                eprintln!("{e:?}");
                return Ok(());
            }
        };

        let (grayscale_json, fullcolor_json): (Vec<Option<MagickJson>>, Vec<Option<MagickJson>>) =
            json.into_iter()
                .map(|json| {
                    if json.image.type_ == "Grayscale" {
                        (Some(json), None)
                    } else {
                        (None, Some(json))
                    }
                })
                .unzip();

        let grayscale_json: Vec<_> = grayscale_json.into_iter().flatten().collect();
        let fullcolor_json: Vec<_> = fullcolor_json.into_iter().flatten().collect();

        if !grayscale_json.is_empty() && !self.grayscale_repo.image_exists(name)? {
            insert_grayscale_vectors(&self.grayscale, &self.grayscale_repo, name, &grayscale_json)?;
        }

        if !fullcolor_json.is_empty() {
            if !self.fullcolor_repo.image_exists(name)? {
                insert_fullcolor_vectors(
                    &self.fullcolor,
                    &self.fullcolor_repo,
                    name,
                    &fullcolor_json,
                )?;
            }

            let json = match magick::identify_grayscale(path) {
                Ok(json) => json,
                Err(e) => {
                    eprintln!("{e}");
                    eprintln!("{e:?}");
                    return Ok(());
                }
            };

            insert_grayscale_vectors(&self.grayscale, &self.grayscale_repo, name, &json)?;
        }

        Ok(())
    }
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Io(_) => write!(f, "Error in filesystem"),
            Self::Magick(_) => write!(f, "Error getting image details"),
            Self::Repo(_) => write!(f, "Error in image repo"),
            Self::Vector(_) => write!(f, "Error in vector repo"),
            Self::PathString => write!(f, "Could not convert Path to String"),
        }
    }
}

impl std::error::Error for Error {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Self::Io(e) => Some(e),
            Self::Magick(e) => Some(e),
            Self::Repo(e) => Some(e),
            Self::Vector(e) => Some(e),
            Self::PathString => None,
        }
    }
}

impl From<std::io::Error> for Error {
    fn from(value: std::io::Error) -> Self {
        Self::Io(value)
    }
}

impl From<MagickError> for Error {
    fn from(value: MagickError) -> Self {
        Self::Magick(value)
    }
}

impl From<RepoError> for Error {
    fn from(value: RepoError) -> Self {
        Self::Repo(value)
    }
}

impl From<TreeError> for Error {
    fn from(value: TreeError) -> Self {
        Self::Vector(value)
    }
}
