use std::path::Path;

use redb::{
    Database, MultimapTableDefinition, ReadableMultimapTable, ReadableTable, TableDefinition,
};
use vectordb::VectorId;

pub(super) struct ImageRepo {
    database: Database,
}

#[derive(Debug)]
pub(super) enum RepoError {
    Commit(redb::CommitError),
    Compact(redb::CompactionError),
    Database(redb::DatabaseError),
    Io(std::io::Error),
    Storage(redb::StorageError),
    Table(redb::TableError),
    Transaction(redb::TransactionError),
}

const IMAGE_MULTIMAP: MultimapTableDefinition<'static, &'static str, VectorId> =
    MultimapTableDefinition::new("image_repo::image_multimap");

const INVERSE_IMAGE_TABLE: TableDefinition<'static, VectorId, &'static str> =
    TableDefinition::new("image_repo::inverse_image_table");

impl ImageRepo {
    pub(super) fn open<P: AsRef<Path>>(directory: P) -> Result<Self, RepoError> {
        std::fs::create_dir_all(directory.as_ref())?;

        let mut database = Database::create(directory.as_ref().join("image-repo.redb"))?;

        database.check_integrity()?;
        database.compact()?;

        let txn = database.begin_write()?;
        txn.open_multimap_table(IMAGE_MULTIMAP)?;
        txn.open_table(INVERSE_IMAGE_TABLE)?;
        txn.commit()?;

        Ok(ImageRepo { database })
    }

    pub(super) fn get_name(&self, vector_id: VectorId) -> Result<Option<String>, RepoError> {
        let txn = self.database.begin_read()?;

        let table = txn.open_table(INVERSE_IMAGE_TABLE)?;

        let name = table
            .get(vector_id)?
            .map(|value| String::from(value.value()));

        Ok(name)
    }

    pub(super) fn image_exists(&self, image: &str) -> Result<bool, RepoError> {
        let txn = self.database.begin_read()?;

        let table = txn.open_multimap_table(IMAGE_MULTIMAP)?;

        let b = table.get(image)?.next().is_some();

        Ok(b)
    }

    pub(super) fn get_vectors(&self, image: &str) -> Result<Vec<VectorId>, RepoError> {
        let txn = self.database.begin_read()?;

        let table = txn.open_multimap_table(IMAGE_MULTIMAP)?;

        let vec = table
            .get(image)?
            .map(|res| res.map(|value| value.value()))
            .collect::<Result<Vec<_>, _>>()?;

        Ok(vec)
    }

    pub(super) fn add_image(&self, image: &str, vector_ids: &[VectorId]) -> Result<(), RepoError> {
        let txn = self.database.begin_write()?;

        let mut image_multimap = txn.open_multimap_table(IMAGE_MULTIMAP)?;
        let mut inverse_image_table = txn.open_table(INVERSE_IMAGE_TABLE)?;

        for vector_id in vector_ids {
            image_multimap.insert(image, vector_id)?;
            inverse_image_table.insert(vector_id, image)?;
        }

        drop(inverse_image_table);
        drop(image_multimap);

        txn.commit()?;

        Ok(())
    }
}

impl From<redb::CommitError> for RepoError {
    fn from(value: redb::CommitError) -> Self {
        Self::Commit(value)
    }
}

impl From<redb::CompactionError> for RepoError {
    fn from(value: redb::CompactionError) -> Self {
        Self::Compact(value)
    }
}

impl From<redb::DatabaseError> for RepoError {
    fn from(value: redb::DatabaseError) -> Self {
        Self::Database(value)
    }
}

impl From<std::io::Error> for RepoError {
    fn from(value: std::io::Error) -> Self {
        Self::Io(value)
    }
}

impl From<redb::StorageError> for RepoError {
    fn from(value: redb::StorageError) -> Self {
        Self::Storage(value)
    }
}

impl From<redb::TableError> for RepoError {
    fn from(value: redb::TableError) -> Self {
        Self::Table(value)
    }
}

impl From<redb::TransactionError> for RepoError {
    fn from(value: redb::TransactionError) -> Self {
        Self::Transaction(value)
    }
}

impl std::fmt::Display for RepoError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Commit(_) => write!(f, "Error committing transaction"),
            Self::Compact(_) => write!(f, "Error in compaction"),
            Self::Database(_) => write!(f, "Error opening database"),
            Self::Io(_) => write!(f, "Error creating database directory"),
            Self::Storage(_) => write!(f, "Error interacting with storage"),
            Self::Table(_) => write!(f, "Error opening table"),
            Self::Transaction(_) => write!(f, "Error openning transaction"),
        }
    }
}

impl std::error::Error for RepoError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Self::Commit(e) => Some(e),
            Self::Compact(e) => Some(e),
            Self::Database(e) => Some(e),
            Self::Io(e) => Some(e),
            Self::Storage(e) => Some(e),
            Self::Table(e) => Some(e),
            Self::Transaction(e) => Some(e),
        }
    }
}
