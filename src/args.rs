use std::path::PathBuf;

#[derive(Debug, clap::Parser)]
// Index and query for image similarities
pub(super) struct Args {
    // A list of paths to index. These can be image files or directories containing image files
    #[clap(short, long)]
    pub(super) index: Vec<PathBuf>,

    // A path to query for similarities. This must be an image file
    #[clap(short, long)]
    pub(super) query: Option<PathBuf>,
}
