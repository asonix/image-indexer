use self::json::MagickJson;

pub mod json;

#[derive(Debug)]
pub(super) enum MagickError {
    Io(std::io::Error),
    Json(serde_json::Error),
}

pub(super) fn identify_grayscale<P: AsRef<std::path::Path>>(
    path: P,
) -> Result<Vec<MagickJson>, MagickError> {
    identify(path, &["-grayscale", "Rec709Luminance"])
}

pub(super) fn identify_fullcolor<P: AsRef<std::path::Path>>(
    path: P,
) -> Result<Vec<MagickJson>, MagickError> {
    identify(path, &[])
}

fn identify<P: AsRef<std::path::Path>>(
    path: P,
    extra_args: &[&'static str],
) -> Result<Vec<MagickJson>, MagickError> {
    let output = std::process::Command::new("magick")
        .args(["convert", "-moments", "-auto-orient", "-colorspace", "sRGB"])
        .args(extra_args)
        .arg(path.as_ref())
        .arg("json:")
        .output()?;

    let json: Vec<_> = match serde_json::from_slice(&output.stdout) {
        Ok(json) => json,
        Err(e) => {
            eprintln!(
                "Couldn't deserialize {:?}: {}",
                path.as_ref(),
                String::from_utf8_lossy(&output.stdout)
            );
            return Err(e.into());
        }
    };

    Ok(json)
}

impl std::fmt::Display for MagickError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Io(_) => write!(f, "Error executing command"),
            Self::Json(_) => write!(f, "Error deserializing output"),
        }
    }
}

impl std::error::Error for MagickError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Self::Io(e) => Some(e),
            Self::Json(e) => Some(e),
        }
    }
}

impl From<std::io::Error> for MagickError {
    fn from(value: std::io::Error) -> Self {
        Self::Io(value)
    }
}

impl From<serde_json::Error> for MagickError {
    fn from(value: serde_json::Error) -> Self {
        Self::Json(value)
    }
}
