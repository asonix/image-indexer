use std::collections::HashMap;

#[derive(Clone, Debug, PartialEq, serde::Deserialize)]
pub struct MagickJson {
    pub version: String,
    pub image: ImageJson,
}

#[derive(Clone, Debug, PartialEq, serde::Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ImageJson {
    pub name: String,
    pub base_name: String,
    pub format: String,
    pub format_description: String,
    pub mime_type: String,
    pub class: Option<String>,
    pub geometry: GeometryJson,
    pub resolution: Option<ResolutionJson>,
    pub print_size: Option<PrintSizeJson>,
    pub units: String,
    #[serde(rename = "type")]
    pub type_: String,
    pub endianness: String,
    pub colorspace: String,
    pub depth: u64,
    pub base_depth: u64,
    pub channel_depth: HashMap<ChannelName, u64>,
    pub pixels: u64,
    pub image_statistics: Option<MaybeOverall>,
    pub channel_statistics: HashMap<ChannelName, StatisticsJson>,
    pub channel_moments: HashMap<ChannelName, ChannelMomentsJson>,
    pub channel_perceptual_hash: ChannelPerceptualHashJson,
    pub rendering_intent: String,
    pub gamma: f32,
    pub chromacity: Option<HashMap<String, ChromacityJson>>,
    pub matte_color: HexString,
    pub background_color: HexString,
    pub border_color: HexString,
    pub transparent_color: HexString,
    pub interlace: String,
    pub intensity: String,
    pub compose: String,
    pub page_geometry: GeometryJson,
    pub dispose: String,
    pub iterations: u64,
    pub compression: String,
    pub quality: Option<u64>,
    pub orientation: String,
    pub properties: HashMap<String, Option<String>>,
    pub artifacts: HashMap<String, String>,
    pub tainted: bool,
    pub filesize: String,
    pub number_pixels: String,
    pub pixels_per_second: String,
    pub user_time: String,
    pub elapsed_time: String,
    pub version: String,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, serde::Deserialize)]
pub struct GeometryJson {
    pub width: u64,
    pub height: u64,
    pub x: u64,
    pub y: u64,
}

#[derive(Clone, Debug, PartialEq, PartialOrd, serde::Deserialize)]
pub struct ResolutionJson {
    pub x: f32,
    pub y: f32,
}

#[derive(Clone, Debug, PartialEq, PartialOrd, serde::Deserialize)]
pub struct PrintSizeJson {
    pub x: f32,
    pub y: f32,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Deserialize)]
pub enum ChannelName {
    #[serde(rename = "alpha")]
    Alpha,
    #[serde(rename = "gray")]
    Gray,
    #[serde(rename = "red")]
    Red,
    #[serde(rename = "green")]
    Green,
    #[serde(rename = "blue")]
    Blue,
}

#[derive(Clone, Debug, PartialEq, PartialOrd, serde::Deserialize)]
#[serde(untagged)]
pub enum MaybeOverall {
    Overall {
        #[serde(rename = "Overall")]
        overall: StatisticsJson,
    },
    Statistics(StatisticsJson),
}

#[derive(Clone, Debug, PartialEq, PartialOrd, serde::Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct StatisticsJson {
    pub min: f32,
    pub max: f32,
    pub mean: f32,
    pub median: f32,
    pub standard_deviation: f32,
    pub kurtosis: f32,
    pub skewness: f32,
    pub entropy: f32,
}

#[derive(Clone, Debug, PartialEq, PartialOrd, serde::Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ChannelMomentsJson {
    pub centroid: CentroidJson,
    pub ellipse_semi_major_minor_axis: EllipseSemiMajorMinorAxisJson,
    pub ellipse_angle: f32,
    pub ellipse_eccentricity: f32,
    #[serde(rename = "I1")]
    pub i1: f32,
    #[serde(rename = "I2")]
    pub i2: f32,
    #[serde(rename = "I3")]
    pub i3: f32,
    #[serde(rename = "I4")]
    pub i4: f32,
    #[serde(rename = "I5")]
    pub i5: f32,
    #[serde(rename = "I6")]
    pub i6: f32,
    #[serde(rename = "I7")]
    pub i7: f32,
    #[serde(rename = "I8")]
    pub i8: f32,
}

#[derive(Clone, Debug, PartialEq, PartialOrd, serde::Deserialize)]
pub struct CentroidJson {
    pub x: f32,
    pub y: f32,
}

#[derive(Clone, Debug, PartialEq, PartialOrd, serde::Deserialize)]
pub struct EllipseSemiMajorMinorAxisJson {
    pub x: f32,
    pub y: f32,
}

#[derive(Clone, Debug, PartialEq, serde::Deserialize)]
pub struct ChannelPerceptualHashJson {
    pub colorspaces: Vec<String>,

    #[serde(flatten)]
    pub hashes: HashMap<String, PerceptualHashJson>,
}

#[derive(Clone, Debug, PartialEq, PartialOrd, serde::Deserialize)]
pub struct PerceptualHashJson {
    #[serde(rename = "PH1")]
    pub ph1: [f32; 2],
    #[serde(rename = "PH2")]
    pub ph2: [f32; 2],
    #[serde(rename = "PH3")]
    pub ph3: [f32; 2],
    #[serde(rename = "PH4")]
    pub ph4: [f32; 2],
    #[serde(rename = "PH5")]
    pub ph5: [f32; 2],
    #[serde(rename = "PH6")]
    pub ph6: [f32; 2],
    #[serde(rename = "PH7")]
    pub ph7: [f32; 2],
}

#[derive(Clone, Debug, PartialEq, PartialOrd, serde::Deserialize)]
pub struct ChromacityJson {
    pub x: f32,
    pub y: f32,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, serde::Deserialize)]
#[serde(transparent)]
pub struct HexString(String);
