{ imagemagick
, lib
, makeWrapper
, nixosTests
, rustPlatform
, Security
, stdenv
}:

rustPlatform.buildRustPackage {
  pname = "image-indexer";
  version = "0.1.0";
  src = ./.;

  cargoLock = {
    lockFile = ./Cargo.lock;
  };

  nativeBuildInputs = [ makeWrapper ];
  buildInputs = lib.optionals stdenv.isDarwin [ Security ];

  postInstall = ''
    wrapProgram $out/bin/image-indexer \
        --prefix PATH : "${lib.makeBinPath [ imagemagick ]}"
  '';

  passthru.tests = { inherit (nixosTests) image-indexer; };

  meta = with lib; {
    description = "A simple image hosting service";
    homepage = "https://git.asonix.dog/asonix/image-indexer";
    license = with licenses; [ agpl3Plus ];
  };
}
